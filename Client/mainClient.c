#include <stdio.h>
#include <stdlib.h>
#include "client.h"

int main() {

	if (InitialisationAvecService("localhost", "13214") != 1) {
		printf("Erreur d'initialisation\n");
		return 1;
	}

	Emission("GET /index.html HTTP/1.1\r\n");
	Emission("Host: localhost\r\n");
	Emission("Accept: text/html\r\n");
	Emission("Accept: text/plain\r\n");
	Emission("User-Agent: Lynx/2.4 libwww/2.1.4\r\n");
	Emission("\r\n");

	char *res = Reception();

	while(res != NULL){
		printf("%s", res);
		res = Reception();
	}
	Terminaison();

	return 0;
}
